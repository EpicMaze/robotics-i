import easygopigo3 as go
pi = go.EasyGoPiGo3()
import numpy as np
import cv2
import time, os


cap = cv2.VideoCapture(0)
time_start = 0

# Initial threshold values
# if file trackbar_defaults.txt doesnt exist than take those
lH = 0
lS = 0
lV = 0
hH = 179
hS = 255
hV = 255
kernel_min = 3


filename = "trackbar_defaults.txt"
threshold_list = list()

if os.path.exists(filename):
    print(f"{filename} found copying thresholds from it...")
    with open(filename, 'r') as fhandle:
        for line in fhandle:
            if line == '':
                continue
            line = line.strip()
            threshold_list.append(int(line))
else:
    print(f"{filename} not found, using default thresholds...")
    threshold_list = [lH, lS, lV, hH, hS, hV, kernel_min]

            
            
            
        




# Trackbar
trackbar_max_val = 255
trackbar_H = "H"
trackbar_S = "S"
trackbar_V = "V"
trackbar_H_max = "H MAx"
trackbar_S_max = "S Max"
trackbar_V_max = "V MAx"
trackbar_window = "trackbar"
trackbar_kernel_max = 21
trackbar_kernel_bil = "Kernel Bilateral size"
trackbar_kernel_med = "Kernel Median size"
trackbar_kernel_gaus = "Kernel Gaussian size"
trackbar_kernel_blur = "Kernel blur size"


# Trackbars create
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_H, trackbar_window, threshold_list[0], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S, trackbar_window, threshold_list[1], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V, trackbar_window, threshold_list[2], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_H_max, trackbar_window, threshold_list[3], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S_max, trackbar_window, threshold_list[4], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V_max, trackbar_window, threshold_list[5], trackbar_max_val, (lambda a: None))


# Gaussian blur
cv2.createTrackbar(trackbar_kernel_gaus, trackbar_window, threshold_list[6], trackbar_kernel_max, (lambda a: None))








# Function for setting trackbar val to odd number as kernel size can be only odd number (gaussian, ..)
def to_odd(x):
    if int(x) % 2 == 0:
        return x+1
    else:
        return x



# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.filterByArea = True
blobparams.maxArea = 99999
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255



#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords


# Pi searching for pillars
need_adjust = True
moving_through = False
uncertainty = 5

 
def find_both(xcenter, keypoints, speed = 30):
    xcoord = keypoints[0].pt[0]
    pi.set_speed(speed)
    if len(keypoints) < 2:
        if xcoord > xcenter:
            pi.spin_right()
        elif xcoord < xcenter:
            pi.spin_left()
    

def drive_forward(l, r, un, xcenter, keypoints, speed=100, t=2):
    print(f"is_dist_same: {is_dist_same(left_dist, right_dist, uncertainty, int(FRAME_WIDTH/2))}")
    if not is_dist_same(left_dist, right_dist, uncertainty, int(FRAME_WIDTH/2)):
        adjust_both(int(FRAME_WIDTH/2), keypoints)
    
    
    pi.set_speed(speed)
    pi.forward()
    time.sleep(t)
    for i in range(5):
        ret, frame = cap.read()
    pi.stop()
    


def adjust_both(xcenter, keypoints, speed = 40):
    pi.set_speed(speed)
    if (keypoints[1].pt[0] >= xcenter) and (keypoints[0].pt[0] >= xcenter):
        pi.stop()
        pi.spin_right()
            
    elif (keypoints[1].pt[0] <= xcenter) and (keypoints[0].pt[0] <= xcenter):
        pi.stop()
        pi.spin_left()
    
    elif (keypoints[1].pt[0] <= xcenter) and (keypoints[0].pt[0] >= xcenter):
        print("Centered")
        pi.stop()
        #drive_forward()
        
        #drive_forward()


def is_dist_same(l, r, un, xcenter):
    if (l > r + un) and (r > l + un):
        return True
    else:
        False



while True:
    ret, frame = cap.read()
    frame = frame[215:265,:,:]
    FRAME_HEIGHT, FRAME_WIDTH = frame.shape[:-1]
    #print(FRAME_HEIGHT)
    if (time_start != 0):
        time_end = time.time()
        time_elapsed = time_end - time_start
        fps = int(1/time_elapsed)
            #Write some text onto the frame
        cv2.putText(frame_blob, f"FPS: {fps}", (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        
    time_start = time.time()
    
    
    
    # Get trackbar vals
    lH = cv2.getTrackbarPos(trackbar_H, trackbar_window)
    lS = cv2.getTrackbarPos(trackbar_S, trackbar_window)
    lV = cv2.getTrackbarPos(trackbar_V, trackbar_window)
    
    hH = cv2.getTrackbarPos(trackbar_H_max, trackbar_window)
    hS = cv2.getTrackbarPos(trackbar_S_max, trackbar_window)
    hV = cv2.getTrackbarPos(trackbar_V_max, trackbar_window)
    
    #bil_kernel = to_odd(cv2.getTrackbarPos(trackbar_kernel_bil, trackbar_window))
    #med_kernel = to_odd(cv2.getTrackbarPos(trackbar_kernel_med, trackbar_window))
    gaus_kernel = to_odd(cv2.getTrackbarPos(trackbar_kernel_gaus, trackbar_window))
    #blur_kernel = to_odd(cv2.getTrackbarPos(trackbar_kernel_blur, trackbar_window))
    #print("THRESH")
    
    
    # Blur the image

    gaus_frame = cv2.GaussianBlur(frame, (gaus_kernel, gaus_kernel), 0)
    #blur_frame = cv2.blur(frame, (blur_kernel, blur_kernel))
    
    # Limits and inRange
    frame_hsv = cv2.cvtColor(gaus_frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([lH, lS, lV])
    upperLimits = np.array([hH, hS, hV])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (1, 1), (FRAME_WIDTH-1, FRAME_HEIGHT-1), (0, 0, 0), 5)
    outimage = cv2.bitwise_and(gaus_frame, gaus_frame, mask = thresholded)
   
    # Blog detection put keypoints and edge detection
    mask = cv2.bitwise_not(outimage)
    
    
    # Dilation 
    #kernel = np.ones((3,3),np.uint8)
    #opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    
    
    
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    frame_blob = cv2.rectangle(frame_blob, (int(FRAME_WIDTH/2), 0), (int(FRAME_WIDTH/2), FRAME_HEIGHT), (0, 0, 255), 1)
    keypoints_coords_str = get_keypoints_coords(keypoints)
    #print("Number of keypoints:", len(keypoints))
    for coord in keypoints_coords_str:
        cv2.putText(frame_blob, str(coord), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
    
    
    """
    _, contours, hierarchy = cv2.findContours(thresholded, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    for cont in contours:
        print(f"Contour: {cont}")
    
    cv2.drawContours(frame, contours, -1, (255, 0, 0), 2)
    """
    
    cv2.imshow(trackbar_window, outimage)
    cv2.imshow("Blob", frame_blob)
    
    # Rotating Pi


    
    # No visible pillars
    if len(keypoints) == 0:
        pi.set_speed(60)
        pi.spin_left()
        print("Looking for keypoints")
        
    
    # One pillar
    if len(keypoints) == 1:
        pi.stop()
        print("Found 1 pillar")
        find_both(xcenter = int(FRAME_WIDTH/2), keypoints = keypoints, speed = 30)
            
    if len(keypoints) == 2:
        
        #print(f"First x: {keypoints[0].pt[0]}\nSecond x: {keypoints[1].pt[0]}")
        xleft, xright = int(keypoints[1].pt[0]), int(keypoints[0].pt[0])
        left_dist = abs(int(FRAME_WIDTH/2) - xleft)
        right_dist = abs(int(FRAME_WIDTH/2) - xright)
   
        drive_forward(left_dist, right_dist, uncertainty, int(FRAME_WIDTH/2), keypoints)
            
        
            
            
            
                
            
        
            
    elif len(keypoints) > 2:
        pi.stop()
        print("Multiple keypoints. Threshold!")
            
            
    
        
    
    
    
    
    """
    if len(keypoints) == 1:
        can_turn = True
        ball_coords = (keypoints[0].pt[0], keypoints[0].pt[1])  
    else:
        can_turn = False
        
        
    if can_turn:
    
        pi.set_speed(50)
        xcoord = ball_coords[0]
        #while (xcoord <= FRAME_WIDTH/2-10) and (xcoord >= FRAME_WIDTH/2+10):
        if (xcoord >= FRAME_WIDTH/2-10) and (xcoord <= FRAME_WIDTH/2+10):
            pi.stop()
            print("Ball found")
        elif xcoord > FRAME_WIDTH/2+10:
            pi.spin_right()
        elif xcoord < FRAME_WIDTH/2-10:
            pi.spin_left()
       
    else:
        print("Cant move, more than one coords, filter properly a**")
    """    
    
    
    
    
    # Quit the program when 'esc' is pressed
    if cv2.waitKey(1) == 27:
        break


    

# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()

# Save thresholds in file
print("Saving thresholds to file")
threshold_list = [lH, lS, lV, hH, hS, hV, gaus_kernel]
with open(filename, "w") as fhandle:
    for th in threshold_list:
        fhandle.write(f"{str(th)}\n")
        

    
    
    

