import cv2
import numpy as np

trackbar_iter = "Erosion Iterations"
trackbar_kernel = "Kernel size"
trackbar_window = "Output"
trackbar_iter_val = 1
trackbar_kernel_val = 3

cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_iter, trackbar_window, trackbar_iter_val, 10, (lambda x: None))
cv2.createTrackbar(trackbar_kernel, trackbar_window, trackbar_kernel_val, 7, (lambda x: None))

img = cv2.imread('Task1.png', 0)




while True:
    # Get trackbar values
    num_iter = cv2.getTrackbarPos(trackbar_iter, trackbar_window)
    kernel_size = cv2.getTrackbarPos(trackbar_kernel, trackbar_window)
    
    # Create kernel with selected size from trackbar
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    # Erosion followed by dilation
    opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel, iterations = num_iter)
    
    # Showing results
    cv2.imshow('Original', img)
    cv2.imshow(trackbar_window, opening)


    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break





cv2.waitKey(0)
cv2.destroyAllWindows()
