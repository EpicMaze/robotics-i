import cv2
import numpy as np

trackbar_eros_iter = "Erosion Iterations"
trackbar_eros_kernel = "Erosion Kernel size"
trackbar_dil_iter = "Dlation Iterations"
trackbar_dil_kernel = "Dilation Kernel size"
trackbar_window = "Output"
trackbar_iter_val = 1
trackbar_kernel_val = 3

cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_dil_iter, trackbar_window, trackbar_iter_val, 15, (lambda x: None))
cv2.createTrackbar(trackbar_dil_kernel, trackbar_window, trackbar_kernel_val, 21, (lambda x: None))
cv2.createTrackbar(trackbar_eros_iter, trackbar_window, trackbar_iter_val, 15, (lambda x: None))
cv2.createTrackbar(trackbar_eros_kernel, trackbar_window, trackbar_kernel_val, 21, (lambda x: None))


def norm_size(x):
    if x <= 0:
        return 1
    else:
        return x
    


img = cv2.imread('Task2.png', 0)



while True:
    # Get trackbar values
    num_eros_iter = cv2.getTrackbarPos(trackbar_eros_iter, trackbar_window)
    kernel_size_eros = norm_size(cv2.getTrackbarPos(trackbar_eros_kernel, trackbar_window))
    num_dil_iter = cv2.getTrackbarPos(trackbar_dil_iter, trackbar_window)
    kernel_size_dil = norm_size(cv2.getTrackbarPos(trackbar_dil_kernel, trackbar_window))
    
    # Create kernel with selected size from trackbar
    #kernel = np.ones((kernel_size, kernel_size), np.uint8)
    kernel_dil = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size_dil, kernel_size_dil))
    kernel_eros = cv2.getStructuringElement(cv2.MORPH_RECT, (kernel_size_eros, kernel_size_eros))
    #kernel = np.array([[0, 0, 0, 0, 1],
    #                   [0, 1, 1, 1, 0],
    #                   [0, 1, 1, 1, 0],
    #                   [0, 1, 1, 1, 0],
    #                   [0, 0, 0, 0, 0]], np.uint8)
    
    # Dilation followed by erosion (Using morph_close)
    
    dilation = cv2.dilate(img, kernel_dil, iterations = num_dil_iter)
    erosion = cv2.erode(dilation, kernel_eros, iterations = num_eros_iter)
    #opening = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel, iterations = num_iter)
    
    # Showing results
    cv2.imshow('Original', img)
    cv2.imshow(trackbar_window, erosion)


    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break





cv2.waitKey(0)
cv2.destroyAllWindows()

