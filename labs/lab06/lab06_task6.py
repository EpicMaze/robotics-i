import easygopigo3 as go
pi = go.EasyGoPiGo3()
import numpy as np
import cv2
import time, os


cap = cv2.VideoCapture(0)
time_start = 0

# Initial threshold values
# if file trackbar_defaults.txt doesnt exist than take those

"""
filename = "trackbar_defaults.txt"
thresholds = [0, 0, 0, 179, 255, 255, 3]


if os.path.exists(filename):
    thresholds.clear()
    print(f"{filename} found copying thresholds from it...")
    with open(filename, 'r') as fhandle:
        for line in fhandle:
            if line == '':
                continue
            line = int(line.strip())
            thresholds.append(line)
else:
    print(f"{filename} not found, using default thresholds...")
"""
            
            

# Trackbar
"""
trackbar_window = "Trackbar"
trackbar_H = "H"
trackbar_S = "S"
trackbar_V = "V"
trackbar_H_max = "H MAx"
trackbar_S_max = "S Max"
trackbar_V_max = "V MAx"
trackbar_kernel_max = 21
trackbar_kernel_gaus = "Kernel Gaussian size"


# Trackbars create
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_H, trackbar_window, thresholds[0], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S, trackbar_window, thresholds[1], 255, (lambda a: None))
cv2.createTrackbar(trackbar_V, trackbar_window, thresholds[2], 255, (lambda a: None))
cv2.createTrackbar(trackbar_H_max, trackbar_window, thresholds[3], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S_max, trackbar_window, thresholds[4], 255, (lambda a: None))
cv2.createTrackbar(trackbar_V_max, trackbar_window, thresholds[5], 255, (lambda a: None))
cv2.createTrackbar(trackbar_kernel_gaus, trackbar_window, thresholds[6], trackbar_kernel_max, (lambda a: None))
"""

# Function for setting trackbar val to odd number as kernel size can be only odd number (gaussian, ..)
def to_odd(x):
    if int(x) % 2 == 0:
        return x+1
    else:
        return x



# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 250
blobparams.filterByArea = True
blobparams.maxArea = 99999
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255



#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

#print(dir(detector))


def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]), int(keypoint.size))
        keypoints_coords.append(coords)
        
    return keypoints_coords


# Pi searching for pillars
need_adjust = True
drive_throught = False

 
def find_both(keypoints):
    xcoord = keypoints[0].pt[0]
    pi.set_speed(30)
    if len(keypoints) < 2:
        if xcoord > FRAME_CENTER:
            pi.spin_right()
            print("Spin right")
        elif xcoord < FRAME_CENTER:
            pi.spin_left()
            print("Spin left")
    


def adjust_to_center(keypoints, need_adjust):
    # FIX-ME
    pi.set_speed(30)
    x_error = 85
    left_coord = min(keypoints[0], keypoints[1])
    right_coord = max(keypoints[0], keypoints[1])
    #right_coord, left_coord = keypoints[0], keypoints[1]
    #if (left_coord[0] <= FRAME_CENTER - x_error) and (right_coord[0] >= FRAME_CENTER + x_error):
    #global need_adjust


    if (left_coord[0] >= FRAME_CENTER - x_error) and (right_coord[0] > FRAME_CENTER + x_error ):
        pi.spin_right()
        print("Adjusting to right")
        
    elif (right_coord[0] <= FRAME_CENTER + x_error) and (left_coord[0] < FRAME_CENTER - x_error ):
        print("Adjusting to left")
        pi.spin_left()
    else:
        print("Centered")
        
    
    

    #print(f"--------\nleft:{left_coord[0]}\nright:{right_coord[0]}\nCenter:{FRAME_CENTER - x_error},{FRAME_CENTER + x_error}\n--------")


def locate_to_center(keypoints, drive_throught, scale):
    #global drive_throught
    pi.set_speed(30)
    left_coord = min(keypoints[0], keypoints[1])
    right_coord = max(keypoints[0], keypoints[1])
    print(f"{left_coord}, {right_coord}")
    size_error = 5 * scale
    print(f"size left:{left_coord[2]}, size_right:{right_coord[2]}")
    if (min(int(left_coord[2]), int(right_coord[2])) + size_error) == max(int(left_coord[2]), int(right_coord[2])):
        print("Drive throught")
        return True

    elif int(left_coord[2]) > int(right_coord[2]):
        print("Spin right 40 degrees")
        #t = time.time()
        return 2
        #print("Forward (cm, time..)")
        #pi.turn_degrees(30)
        #pi.set_motor_dps(pi.MOTOR_LEFT, 90)
        #pi.set_motor_dps(pi.MOTOR_RIGHT, -90)

    elif int(left_coord[2]) < int(right_coord[2]):
        print("Spin left 40 degrees")
        #pi.spin_left()
        #time.sleep(5)
        #print("Forward (cm, time..)")
        return 1


    

size_scale = 1

while True:
    ret, frame = cap.read()
    frame = frame[235:245,:,:]
    FRAME_HEIGHT, FRAME_WIDTH = frame.shape[:-1]
    FRAME_CENTER = int(FRAME_WIDTH/2)
    #print(FRAME_HEIGHT)
    
    # FPS
    """
    if (time_start != 0):
        time_end = time.time()
        time_elapsed = time_end - time_start
        fps = int(1/time_elapsed)
            #Write some text onto the frame
        cv2.putText(frame_blob, f"FPS: {fps}", (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        
    time_start = time.time()
    """
    
    
    # Get trackbar vals
    """
    lH = cv2.getTrackbarPos(trackbar_H, trackbar_window)
    lS = cv2.getTrackbarPos(trackbar_S, trackbar_window)
    lV = cv2.getTrackbarPos(trackbar_V, trackbar_window)
    
    hH = cv2.getTrackbarPos(trackbar_H_max, trackbar_window)
    hS = cv2.getTrackbarPos(trackbar_S_max, trackbar_window)
    hV = cv2.getTrackbarPos(trackbar_V_max, trackbar_window)
    
    gaus_kernel = to_odd(cv2.getTrackbarPos(trackbar_kernel_gaus, trackbar_window))
    """
    
    # Blur the image
    #gaus_frame = cv2.GaussianBlur(frame, (13, 13), 0)
    

    
    
    # Limits and inRange
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([0, 208, 102])
    upperLimits = np.array([15, 255, 215])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (1, 1), (FRAME_WIDTH-1, FRAME_HEIGHT-1), (0, 0, 0), 1)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
   
    # Blog detection put keypoints and edge detection
    mask = cv2.bitwise_not(outimage)
    
    # Blob detection
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    frame_blob = cv2.rectangle(frame_blob, (int(FRAME_WIDTH/2), 0), (int(FRAME_WIDTH/2), FRAME_HEIGHT), (0, 0, 255), 1)
    keypoints_coords = get_keypoints_coords(keypoints)
    #print("keypoints", keypoints[0].response, keypoints[1].response)

    #print("Number of keypoints:", len(keypoints))
    for coord in keypoints_coords:
        cv2.putText(frame_blob, str(coord[:-1]), coord[:-1], cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 3)
    
    
    
    #cv2.imshow(trackbar_window, outimage)
    cv2.imshow("Blob", frame_blob)
    
    
          
    # Look for keypoints, adjust, drive thought pillars Pi
    
    # No visible pillars
    if len(keypoints) == 0:
        pi.set_speed(60)
        pi.spin_left()
        print("Looking for keypoints")
        
    
    # One pillar
    if len(keypoints) == 1:
        #pi.stop()
        print("Found 1 pillar. Looking for both")
        find_both(keypoints)
    
    
    if len(keypoints) == 2:
        #print(need_adjust)
        #need_adjust = True
        #min_coord = min(keypoints_coords[0], keypoints_coords[1])
        #print(min_coord)
        #adjust_to_center(keypoints = keypoints_coords)
        #need_adjust = True
        
        adjust_to_center(keypoints_coords, need_adjust)
        
        
        
        
        locate_cond = locate_to_center(keypoints_coords, drive_throught, size_scale)
        
        pi.set_speed(60)
        if locate_cond == 1:
            size_scale += 0.2
            # Left 30 deg and drive
            pi.turn_degrees(-90)
            pi.stop()
            pi.forward()
            time.sleep(4)
            for i in range(6):
                ret, frame = cap.read()
        
        elif locate_cond == 2:
            size_scale += 0.2
            # Right 30 deg and drive
            pi.turn_degrees(90)
            pi.stop()
            pi.forward()
            time.sleep(4)
            for i in range(6):
                ret, frame = cap.read()
        
        elif locate_cond:
            drive_throught = locate_cond
            print("DRIVING THROUGHT")
            
            break

        
     
        
    elif len(keypoints) > 2:
        pi.stop()
        print("Multiple keypoints. Threshold!")
    
    
    
    
    
    # Quit the program when 'esc' is pressed
    if cv2.waitKey(1) == 27:
        break


if drive_throught:
    print("Driving!")
    pi.set_speed(50)
    pi.drive_cm(70)
    pi.stop()




# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()
cap.release()


# Save thresholds in file
"""
print("Saving thresholds to file")
thresholds = [lH, lS, lV, hH, hS, hV, gaus_kernel]
with open(filename, "w") as fhandle:
    for th in thresholds:
        fhandle.write(f"{str(th)}\n")
        

"""
    
    

