import cv2
import numpy as np
cap = cv2.VideoCapture(0)




def keypoints_to_dict(keypoints):
    if not isinstance(keypoints, list):
        raise Exception(f"Object {type(keypoints)} is not instance of list !")
        return None
    keypoints_dict = dict()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_dict[coords] = int(keypoint.size)
        
        
    return keypoints_dict

    
# Blog params
blobparams = cv2.SimpleBlobDetector_Params()

blobparams.minArea = 100
blobparams.maxArea = 99999
blobparams.filterByArea = True

blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False


#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

while True:
    ret, frame = cap.read()
    FRAME_HEIGHT, FRAME_WIDTH = frame.shape[:-1]
    FRAME_CENTER = int(FRAME_WIDTH/2)
    
    
    # Thresholding
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([0, 208, 102])
    upperLimits = np.array([15, 255, 215])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (1, 1), (FRAME_WIDTH-1, FRAME_HEIGHT-1), (0, 0, 0), 1)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
   
   
    # Blog detection ==> keypoints: list()
    mask = cv2.bitwise_not(outimage)
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # Red line in the middle of the screen to visually see the center.
    frame_blob = cv2.rectangle(frame_blob, (int(FRAME_WIDTH/2), 0), (int(FRAME_WIDTH/2), FRAME_HEIGHT), (0, 0, 255), 1)
    
    
    # Variable to hold keypoints ==> keypoints_dict[coord] = size
    keypoints_dict = keypoints_to_dict(keypoints)
    

    #print("Number of keypoints:", len(keypoints))
    for coord in keypoints_dict:
        cv2.putText(frame_blob, str(coord[:-1]), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
    
    
    
    cv2.imshow("Blob", frame_blob)
    
    
       # Quit the program when 'esc' is pressed
    if cv2.waitKey(1) == 27:
        break



# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()
cap.release()
    
    