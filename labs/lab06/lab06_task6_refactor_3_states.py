
import easygopigo3 as go
pi = go.EasyGoPiGo3()

import numpy as np
import cv2

import time, os


cap = cv2.VideoCapture(0)




def keypoints_to_dict(keypoints):
    if not isinstance(keypoints, list):
        raise Exception(f"Object {type(keypoints)} is not instance of list !")
        return None
    keypoints_dict = dict()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_dict[coords] = int(keypoint.size)
        
        
    return keypoints_dict




def is_in_center(keypoints_dict):
    if not isinstance(keypoints_dict, dict):
        raise Exception(f"Object {type(keypoints)} is not instance of dictionary !")
        return None
    
    keypoints_coords = list(keypoints_dict.keys())
    #print("COODS", keypoints_coords)
    x_left = min(keypoints_coords[0], keypoints_coords[1])
    x_right = max(keypoints_coords[0], keypoints_coords[1])
    x_error = 5
    x_average = int(np.mean([x_left[0], x_right[0]]))
    #print(x_average, FRAME_CENTER)
    if (x_average >= FRAME_CENTER - x_error) and (x_average <= FRAME_CENTER + x_error):
        return True
    else:
        return False




def locate_current_side(keypoints_dict):
    if not isinstance(keypoints_dict, dict):
        raise Exception(f"Object {type(keypoints)} is not instance of dictionary !")
        return None
    keypoints_coords = list(keypoints_dict.keys())
    x_left = min(keypoints_coords[0], keypoints_coords[1])
    x_right = max(keypoints_coords[0], keypoints_coords[1])
    left_size = keypoints_dict[x_left]
    right_size = keypoints_dict[x_right]
    max_size = max(left_size, right_size)
    min_size = min(left_size, right_size)
    print(min_size, max_size)
    
    # ! ---- TO-DO: Ration, 38, 4 ==> determine ----
    if (min_size/max_size) > 0.95:
        located_on = "C"
        print("Robot located in center of both pillars")
        """
        if (max_size >= 40 - 2) and (max_size <= 40 + 2):
            print("Close enough just drive_cm")
            pi.set_speed(60)
            pi.drive_cm(100)
        else:
        """
        """
        if (max_size >= 36 - 2) and (max_size <= 36 + 2):
            if left_size > right_size:
                located_on = "L"
                print("Robot located on left of both pillars")
    
            elif left_size < right_size:
                located_on = "R"
                print("Robot located in center of both pillars")
            print("")
        """
        
    elif left_size > right_size:
        located_on = "L"
        print("Robot located on left of both pillars")
    
    elif left_size < right_size:
        located_on = "R"
        print("Robot located on right of both pillars")
        
    
    
        
    
    return located_on
        
    
    

def adjust_robot_pos(located_on, state):
    adjust_deg = 50
    adjust_cm = 20
    if located_on == "L":
        print("Adjusting to right, driving a bit")
        pi.turn_degrees(adjust_deg)
        pi.drive_cm(adjust_cm)
        state = states_list[0]
        
    elif located_on == "R":
        print("Adjusting to left, driving a bit")
        pi.turn_degrees(-1 * adjust_deg)
        pi.drive_cm(adjust_cm)
        state = states_list[0]
        
    elif located_on == "C":
        print("Drive and adjust")
        state = states_list[2]
        
    return state
        
        

def proportional_controller(mean_loc):
    gospeed = 300
    error = FRAME_CENTER - mean_loc
    Kp = 1.6
    
    P = error * Kp
    lwheel = gospeed + P
    rwheel = gospeed - P
    print(lwheel, rwheel)
    
    pi.set_motor_dps(pi.MOTOR_LEFT, lwheel)
    pi.set_motor_dps(pi.MOTOR_RIGHT, rwheel)
    
    return
    
        
    
def bang_bang_error(mean_loc):
    pi.set_speed(60)
    
    if mean_loc < FRAME_CENTER - 5:
        pi.left()
        
    elif mean_loc > FRAME_CENTER + 5:
        pi.right()
    
    

    
    
# Blog params
blobparams = cv2.SimpleBlobDetector_Params()

blobparams.minArea = 150
blobparams.maxArea = 99999
blobparams.filterByArea = True

blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False


#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)    
    



# State machines
states_list = ('Searching', 'Both found and centered', 'Driving and Adjusting')
current_state = 'Searching'

while True:
    ret, frame_full = cap.read()
    frame = frame_full[235:245,:,:]
    FRAME_HEIGHT, FRAME_WIDTH = frame.shape[:-1]
    FRAME_CENTER = int(FRAME_WIDTH/2)
    #print(FRAME_HEIGHT)


    
    # Thresholding
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    #lowerLimits = np.array([0, 208, 102])
    #upperLimits = np.array([15, 255, 215])
    lowerLimits = np.array([1, 190, 142])
    upperLimits = np.array([15, 255, 247])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (1, 1), (FRAME_WIDTH-1, FRAME_HEIGHT-1), (0, 0, 0), 1)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
   
   
    # Blog detection ==> keypoints: list()
    mask = cv2.bitwise_not(outimage)
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # Red line in the middle of the screen to visually see the center.
    frame_blob = cv2.rectangle(frame_blob, (int(FRAME_WIDTH/2), 0), (int(FRAME_WIDTH/2), FRAME_HEIGHT), (0, 0, 255), 1)
    
    
    # Variable to hold keypoints ==> keypoints_dict[coord] = size
    keypoints_dict = keypoints_to_dict(keypoints)
    

    #print("Number of keypoints:", len(keypoints))
    for coord in keypoints_dict:
        cv2.putText(frame_blob, str(keypoints_dict[coord]), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
        #print(keypoints_dict[coord])
    
    
    cv2.imshow("Blob", frame_blob)
    
    #pi.set_speed(50)
    # Look for keypoints, adjust, drive thought pillars Pi
    
    if current_state == states_list[0]:
        # No visible pillars
        if len(keypoints) < 2:
            pi.set_speed(50)
            pi.spin_left()
            print("Looking for keypoints")
        
        elif len(keypoints) == 2:
            pi.set_speed(30)
            pi.spin_left()
            #len(keypoints_dict)
            if is_in_center(keypoints_dict):
                print("Centered")
                pi.stop()
                current_state = states_list[1]
        
        else:
            print("Too many threshold! Stop...")
            pi.stop()
        
    if current_state == states_list[1]:
        pi.set_speed(60)
        # Detect which side are we (Left, Right, Center) in respect to pillars line
        located_on = locate_current_side(keypoints_dict)
        cap.read()
        cap.read()
        cap.read()
        cap.read()
        cap.read()
        # Handle adjusting based on which side we are located
        current_state = adjust_robot_pos(located_on, current_state)
        # Fill camera buffer with fresh frames
        cap.read()
        cap.read()
        cap.read()
        cap.read()
        cap.read()
        
        
        
        
        # Reset state
        #current_state = states_list[0]
    
    if current_state == states_list[2]:
        #pi.set_speed(60)
        if len(keypoints) == 2:    
            # Get mean_loc, which is linelocation refering to lab07
            keypoints_coords = list(keypoints_dict.keys())
            x_left = min(keypoints_coords[0], keypoints_coords[1])
            x_right = max(keypoints_coords[0], keypoints_coords[1])
            x_average = int(np.mean([x_left[0], x_right[0]]))
            bang_bang_error(x_average)
            #proportional_controller(x_average)
        
        
        else:
            print("Close anougth just for driving")
            pi.drive_cm(100)
            cap.read()
            cap.read()
            cap.read()
            cap.read()
            cap.read()
            break
        
        
        
    
    
    
    # Quit the program when 'esc' is pressed
    if cv2.waitKey(1) == 27:
        break



# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()
cap.release()




    
    



