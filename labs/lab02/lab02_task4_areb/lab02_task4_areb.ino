#include <Servo.h>

Servo my_servo;

int dps = 1;
int angle = 0;
bool goClockWise = true;

void setup() {
  //Serial.begin(9600);
  // put your setup code here, to run once:
  my_servo.attach(3);
  
  
}

void loop() {
  // put your main code here, to run repeatedly:
  //my_servo.write(360);
  //currentAngle = my_servo.read();
  if (goClockWise == true) {
    angle++;
    my_servo.write(angle*dps);
    if (angle >= 180) {goClockWise = false;}
  } else {
   angle--;
   my_servo.write(angle*dps);
   if (angle <= 0) {goClockWise = true;}
  }

 delay(10);

  
}
