#include <Servo.h>

Servo my_servo;

int dps = 1;
int angle = 0;
bool goClockWise = true;
bool canScan = true;

int echo_pin = 2;
int trig_pin = 3;
int delay_us = 10; // <--- YOU HAVE TO FIND THE CORRECT VALUE FROM THE DATASHEET
long distance_mm = 0;
long duration_us;
int velocity = 340;


void setup() {
  // and set echo_pin and trigar_pin to correct modes
  Serial.begin(9600);
  pinMode(echo_pin, INPUT);
  pinMode(trig_pin, OUTPUT);
  my_servo.attach(4);

}

void loop() {
  // put your main code here, to run repeatedly:


  digitalWrite(trig_pin, HIGH);
  delayMicroseconds(delay_us);
  digitalWrite(trig_pin, LOW);
  // Read the pulse HIGH state on echo_pin
  // the length of the pulse in microseconds
  duration_us = pulseIn(echo_pin, HIGH);

  distance_mm = duration_us * velocity / 2000;
  //if (distance_mm < 300) {canScan = false;} else {canScan = true;}
  if (distance_mm > 300) {
    if (goClockWise == true) {
      angle++;
      my_servo.write(angle * dps);
      if (angle >= 180) {
        goClockWise = false;
      }
    } else {
      angle--;
      my_servo.write(angle * dps);
      if (angle <= 0) {
        goClockWise = true;
      }
    }
  } else {
    Serial.print("Distance too close, stopped scanning: ");
    Serial.println(distance_mm);
  }





  delay(10);
}
