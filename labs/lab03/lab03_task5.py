#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
import math
# reference pins by GPIO numbers
GPIO.setmode(GPIO.BCM)
# disable warnings
GPIO.setwarnings(False)

# define row and column pin numbers
row_pins = [21, 20, 16, 19, 13, 6, 5]
#row_pins = [5, 6, 13, 19, 16, 20, 21]
col_pins = [2, 3, 4, 14, 15]

# set all the pins as outputs and set column pins high, row pins low
GPIO.setup(col_pins, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(row_pins, GPIO.OUT, initial=GPIO.HIGH)

# Sets the waiting time between rows. With larger wait times (0.1) you can see that rows are lit up at different times. With smaller times (0.01) the LEDs appear to be not blinking at all
wait_time = 1

def show_row(row_number, columns, delay):
    #row_number_arr = abs(row_number - len(row_pins))
    for col in columns:
        GPIO.output(col_pins[col-1], GPIO.HIGH)
        GPIO.output(row_pins[row_number-1], GPIO.LOW)
        # wait
        time.sleep(delay)
        # set the pins back to previous states
        GPIO.output(col_pins[col-1], GPIO.LOW)
        GPIO.output(row_pins[row_number-1], GPIO.HIGH)
    #print(col_pins)
    # Control a row of the dot matrix display
    # YOUR CODE GOES HERE:
    
    
"""
GPIO.output(col_pins[2], GPIO.HIGH)
GPIO.output(row_pins[2], GPIO.LOW)
# wait
time.sleep(delay)
# set the pins back to previous states
GPIO.output(col_pins[2], GPIO.LOW)
GPIO.output(row_pins[2], GPIO.HIGH)
"""
delay = 0.001
for i in range(1000):    
    show_row(6, [2, 3, 5], delay)
    
print('name')
time.sleep(1)


for i in range(400):
    show_row(1, [2, 3, 4], delay)
    show_row(2, [1, 5], delay)
    show_row(3, [1, 5], delay)
    show_row(4, [1, 2, 3, 4, 5], delay)
    show_row(5, [1, 5], delay)
    show_row(6, [1, 5], delay)
    show_row(7, [1, 5], delay)

print('surname')
time.sleep(1)

for i in range(400):
    show_row(1, [1, 2, 3, 4], delay)
    show_row(2, [1, 5], delay)
    show_row(3, [1, 5], delay)
    show_row(4, [1, 2, 3, 4], delay)
    show_row(5, [1, 3], delay)
    show_row(6, [1, 4], delay)
    show_row(7, [1, 5], delay)

    
    
    

"""

# Displays image 50 times
for i in range(50):
  
  
    # sets column number 3 high
    GPIO.output(col_pins[3], GPIO.HIGH)
    # sets row number 4 low, this should light up the middle LED
    GPIO.output(row_pins[1], GPIO.LOW)

    # wait
    time.sleep(wait_time)

    # set the pins back to previous states
    GPIO.output(col_pins[3], GPIO.LOW)
    GPIO.output(row_pins[1], GPIO.HIGH)

    # sets columns number 2, 4 high
    GPIO.output(col_pins[1], GPIO.HIGH)
    GPIO.output(col_pins[3], GPIO.HIGH)

    # sets row number 5 low
    GPIO.output(row_pins[4], GPIO.LOW)

    # wait
    time.sleep(wait_time)

    # set the pins back to previous states
    GPIO.output(col_pins[1], GPIO.LOW)
    GPIO.output(col_pins[3], GPIO.LOW)
    GPIO.output(row_pins[4], GPIO.HIGH)
    """
# reset GPIO
GPIO.cleanup()

