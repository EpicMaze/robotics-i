import board
import busio
import adafruit_character_lcd.character_lcd_rgb_i2c as character_lcd

lcd_columns = 16
lcd_rows = 2

i2c = busio.I2C(board.SCL, board.SDA)
lcd = character_lcd.Character_LCD_RGB_I2C(i2c, lcd_columns, lcd_rows)

lcd.color = [0, 90, 10]

while True:
    if lcd.select_button:
        lcd.clear()
        lcd.message = 'true'
    else:
        lcd.clear()
        lcd.message = 'false'
