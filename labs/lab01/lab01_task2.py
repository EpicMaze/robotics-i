import easygopigo3 as go
import time
myRobot = go.EasyGoPiGo3()

def task2(dist=5):
    myRobot.set_speed(300)
    print(f"Selected distance: {dist} cm")
    for i in range(4):
        myRobot.drive_cm(dist)
        myRobot.orbit(-90, 0)
        #myRobot.stop()
        
    print('Task 2 done')
    
task2(40)