import easygopigo3 as go
import time


#Create an instance of the robot with a constructor from the easygopigo3 module that was imported as "go".
myRobot = go.EasyGoPiGo3()

def task1():
    myRobot.set_speed(600)
    for i in range(5):
        myRobot.forward()
        time.sleep(1)
        myRobot.backward()
        time.sleep(1)
        myRobot.stop()
        print(f"{i+1}th iteration done")
        
    print('Task 1 done')
    

    
task1()

        
        

