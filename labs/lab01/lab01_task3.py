import easygopigo3 as go
import time

def led_name(name):
    if name == "left":
        return 0
    elif name == "right":
        return 1
    
def led_on_both():
    myRobot.led_on(1)
    myRobot.led_on(0)

def led_off_both():
    myRobot.led_off(1)
    myRobot.led_off(0)
    

myRobot = go.EasyGoPiGo3()
led_off_both()


myRobot.set_speed(300)
led_on_both()
#myRobot.led_on(led_name("left"))
#myRobot.led_on(led_name("right"))
myRobot.drive_cm(33)
myRobot.led_off(led_name("left"))
myRobot.orbit(68, 0)
#myRobot.led_on(1)
myRobot.led_on(led_name("left"))
myRobot.drive_cm(40)
#myRobot.led_on(1)
myRobot.led_off(led_name("right"))
myRobot.orbit(-30)
myRobot.led_off(led_name("left"))
myRobot.led_on(led_name("right"))
myRobot.orbit(70, 46)

led_off_both()




