
import numpy as np
import cv2
import time, os


cap = cv2.VideoCapture(0)
time_start = 0

# Initial threshold values
# if file trackbar_defaults.txt doesnt exist than take those
lH = 0
lS = 0
lV = 0
hH = 179
hS = 255
hV = 255



filename = "trackbar_blyat.txt"
threshold_list = list()

if os.path.exists(filename):
    print(f"{filename} found copying thresholds from it...")
    with open(filename, 'r') as fhandle:
        for line in fhandle:
            if line == '':
                continue
            line = line.strip()
            threshold_list.append(int(line))
else:
    print(f"{filename} not found, using default thresholds...")
    threshold_list = [lH, lS, lV, hH, hS, hV]

            
            
            
        




# Trackbar
trackbar_max_val = 255
trackbar_H = "H"
trackbar_S = "S"
trackbar_V = "V"
trackbar_H_max = "H MAx"
trackbar_S_max = "S Max"
trackbar_V_max = "V MAx"
trackbar_window = "trackbar"

# Trackbars create
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_H, trackbar_window, threshold_list[0], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S, trackbar_window, threshold_list[1], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V, trackbar_window, threshold_list[2], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_H_max, trackbar_window, threshold_list[3], 179, (lambda a: None))
cv2.createTrackbar(trackbar_S_max, trackbar_window, threshold_list[4], trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V_max, trackbar_window, threshold_list[5], trackbar_max_val, (lambda a: None))




# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.filterByArea = True
blobparams.maxArea = 999999
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255



#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords


while True:
    ret, frame = cap.read()
    
      # Get trackbar vals
    lH = cv2.getTrackbarPos(trackbar_H, trackbar_window)
    lS = cv2.getTrackbarPos(trackbar_S, trackbar_window)
    lV = cv2.getTrackbarPos(trackbar_V, trackbar_window)
    
    hH = cv2.getTrackbarPos(trackbar_H_max, trackbar_window)
    hS = cv2.getTrackbarPos(trackbar_S_max, trackbar_window)
    hV = cv2.getTrackbarPos(trackbar_V_max, trackbar_window)
    #print("THRESH")
    
    # Limits and inRange
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([lH, lS, lV])
    upperLimits = np.array([hH, hS, hV])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
   
    # Blog detection put keypoints and edge detection
    mask = cv2.bitwise_not(outimage)
    # Dilation 
    #kernel = np.ones((3,3),np.uint8)
    #opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    keypoints_coords_str = get_keypoints_coords(keypoints)
    #print(len(keypoints_coords_str))
    for coord in keypoints_coords_str:
        cv2.putText(frame_blob, str(coord), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
    
    
    
    cv2.imshow(trackbar_window, outimage)
    cv2.imshow("Blob", frame_blob)
     
     
    
    
    
    
    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


    

# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()

# Save thresholds in file
print("Saving thresholds to file")
threshold_list = [lH, lS, lV, hH, hS, hV]
with open(filename, "w") as fhandle:
    for th in threshold_list:
        fhandle.write(f"{str(th)}\n")
        

    
    
    