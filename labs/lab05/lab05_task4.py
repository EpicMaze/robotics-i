
import numpy as np
import cv2
import time

# Read video frames
cap = cv2.VideoCapture(0)
time_start = 0

# Colour detection limits initial
lH = 0
lS = 0
lV = 0
hH = 179
hS = 255
hV = 255


# Trackbar values
trackbar_max_val = 255
trackbar_H = "H"
trackbar_S = "S"
trackbar_V = "V"
trackbar_H_max = "H MAx"
trackbar_S_max = "S Max"
trackbar_V_max = "V MAx"
trackbar_window = "trackbar"

# Trackbars create
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_H, trackbar_window, lH, 179, (lambda a: None))
cv2.createTrackbar(trackbar_S, trackbar_window, lS, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V, trackbar_window, lV, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_H_max, trackbar_window, hH, 179, (lambda a: None))
cv2.createTrackbar(trackbar_S_max, trackbar_window, hS, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V_max, trackbar_window, hV, trackbar_max_val, (lambda a: None))




# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.maxArea = 99999
blobparams.filterByArea = True
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255



#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords







while True:
    
    # FPS
    ret, frame = cap.read()
    if (time_start != 0):
        time_end = time.time()
        time_elapsed = time_end - time_start
        fps = int(1/time_elapsed)
           
        #Write some text onto the frame
        cv2.putText(frame_blob, f"FPS: {fps}", (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
        
    time_start = time.time()
    
    # Get trackbar vals
    lH = cv2.getTrackbarPos(trackbar_H, trackbar_window)
    lS = cv2.getTrackbarPos(trackbar_S, trackbar_window)
    lV = cv2.getTrackbarPos(trackbar_V, trackbar_window)
    
    hH = cv2.getTrackbarPos(trackbar_H_max, trackbar_window)
    hS = cv2.getTrackbarPos(trackbar_S_max, trackbar_window)
    hV = cv2.getTrackbarPos(trackbar_V_max, trackbar_window)
    
    # Limits and inRange
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([lH, lS, lV])
    upperLimits = np.array([hH, hS, hV])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
   
    # Blog detection put keypoints and edge detection
    mask = cv2.bitwise_not(outimage)
    # Dilation 
    #kernel = np.ones((3,3),np.uint8)
    #opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
    
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    keypoints_coords_str = get_keypoints_coords(keypoints)
    print(len(keypoints_coords_str))
    for coord in keypoints_coords_str:
        cv2.putText(frame_blob, str(coord), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
    
    
    
    cv2.imshow(trackbar_window, outimage)
    cv2.imshow("Blob", frame_blob)
    
    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()

