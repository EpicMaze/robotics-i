#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np


trackbar_max_val = 255
trackbar_name = "Trackbar to change threshold"
trackbar_window = "trackbar"

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords
        
        



cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_name, trackbar_window, 127, trackbar_max_val, (lambda a: None))

img = cv2.imread('sample01.tiff')
img_keypoints = img.copy()
img_greyscale = cv2.imread('sample01.tiff', 0)


# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.filterByArea = True
blobparams.maxArea = 99999
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255

#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords






while True:
    img_keypoints = img.copy()
    # Getting trackbar position --> value
    threshold_val = cv2.getTrackbarPos(trackbar_name, trackbar_window)
    
    #Thresholding the image (Refer to opencv.org for more details)
    ret, thresh = cv2.threshold(img_greyscale, threshold_val, 255, cv2.THRESH_BINARY)
    
    # Keypoints, using threshold img as mask
    ret2, mask = cv2.threshold(img_greyscale, threshold_val, 255, cv2.THRESH_BINARY)
    keypoints = detector.detect(mask)
    keypoints_coords_str = get_keypoints_coords(keypoints)
    img_keypoints = cv2.drawKeypoints(img_keypoints, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    #print(get_keypoints_coords(keypoints))
    print(len(keypoints_coords_str))
    for coord in keypoints_coords_str:
        cv2.putText(img_keypoints, str(coord), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
   
    
    # Displaying thresholded image on same window as trackbar, so they are on the same place.
    cv2.imshow(trackbar_window, thresh)
    cv2.imshow('Blob detection', img_keypoints)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
   




cv2.waitKey(0)
cv2.destroyAllWindows()


