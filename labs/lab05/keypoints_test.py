#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import numpy as np


trackbar_max_val = 255
trackbar_name = "Trackbar to change threshold"
trackbar_window = "trackbar"

def func():
    pass




img = cv2.imread('sample01.tiff')
img_keypoints = img.copy()
img_greyscale = cv2.imread('sample01.tiff', 0)


#Thresholding the image (Refer to opencv.org for more details)
ret, thresh = cv2.threshold(img_greyscale, 71, 255, cv2.THRESH_BINARY)


#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create()
mask = cv2.bitwise_not(img_keypoints)
keypoints = detector.detect(thresh)
#cv2.drawKeypoints(img_keypoints, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

print(keypoints)



# Displaying thresholded image on same window as trackbar, so they are on the same place.

img_keypoints = cv2.drawKeypoints(img_keypoints, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

   
cv2.imshow('orig', img)
cv2.imshow('keypoints', img_keypoints)


cv2.waitKey(0)
cv2.destroyAllWindows()



