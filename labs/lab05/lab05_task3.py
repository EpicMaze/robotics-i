#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import time

cap = cv2.VideoCapture(0)
time_prev = 0

# Colour detection limits initial
lB = 125
lG = 125
lR = 125
hB = 255
hG = 255
hR = 255


# Trackbar values
trackbar_max_val = 255
trackbar_blue = "Blue"
trackbar_green = "Green"
trackbar_red = "Red"
trackbar_blue_max = "Blue MAx"
trackbar_green_max = "Green Max"
trackbar_red_max = "Red MAx"
trackbar_window = "trackbar"


# Creating trackbar
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_blue, trackbar_window, lB, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_green, trackbar_window, lG, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_red, trackbar_window, lR, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_blue_max, trackbar_window, hB, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_green_max, trackbar_window, hG, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_red_max, trackbar_window, hR, trackbar_max_val, (lambda a: None))




# Blog params
blobparams = cv2.SimpleBlobDetector_Params()
blobparams.minArea = 300
blobparams.filterByArea = True
blobparams.maxArea = 99999
#blobparams.minDistBetweenBlobs = 100
blobparams.filterByCircularity = False
blobparams.filterByInertia = False
blobparams.filterByConvexity = False
#blobparams.filterByColor = True
#blobparams.blobColor = 255



#SimpleBlodDetector
detector = cv2.SimpleBlobDetector_create(blobparams)

def get_keypoints_coords(keypoints):
    keypoints_coords = list()
    for keypoint in keypoints:
        coords = (int(keypoint.pt[0]), int(keypoint.pt[1]))
        keypoints_coords.append(coords)
        
    return keypoints_coords



while True:
    
    # FPS
    ret, frame = cap.read()
   
    time_current = time.time()
    
    time_elapsed = time_current - time_prev
    fps = round(1/time_elapsed, 1)
           
    #Write some text onto the frame
    cv2.putText(frame, f"FPS: {fps}", (5, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)
        
    time_prev = time_current
    
    
    # You will need this later
    # frame = cv2.cvtColor(frame, ENTER_CORRECT_CONSTANT_HERE)
    blue_val = cv2.getTrackbarPos(trackbar_blue, trackbar_window)
    green_val = cv2.getTrackbarPos(trackbar_green, trackbar_window)
    red_val = cv2.getTrackbarPos(trackbar_red, trackbar_window)
    
    blue_val_max = cv2.getTrackbarPos(trackbar_blue_max, trackbar_window)
    green_val_max = cv2.getTrackbarPos(trackbar_green_max, trackbar_window)
    red_val_max = cv2.getTrackbarPos(trackbar_red_max, trackbar_window)
    
    
    
    lowerLimits = np.array([blue_val, green_val, red_val])
    upperLimits = np.array([blue_val_max, green_val_max, red_val_max])

    # Our operations on the frame come here
    thresholded = cv2.inRange(frame, lowerLimits, upperLimits)
    color_th_output = cv2.bitwise_and(frame, frame, mask = thresholded)


    # Blog detection put keypoints and edge detection
    mask = cv2.bitwise_not(color_th_output)
    
    keypoints = detector.detect(mask)
    frame_blob = cv2.drawKeypoints(frame, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    keypoints_coords_str = get_keypoints_coords(keypoints)
    print(len(keypoints_coords_str))
    for coord in keypoints_coords_str:
        cv2.putText(frame_blob, str(coord), coord, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
        
    
    
  
   
    
    cv2.imshow(trackbar_window, color_th_output)
    cv2.imshow('Blob', frame_blob)

    # Display the resulting frame
    #cv2.imshow(trackbar_window, output)

    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
print('closing program')
cv2.destroyAllWindows()


