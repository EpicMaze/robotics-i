#include <Wire.h>
#include <LSM6.h>
#include <Servo.h>

Servo servo;

const int servo_pin = 3;
LSM6 imu;
float tilt_a;

int res;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  servo.attach(servo_pin);
  Wire.begin();
  //LSH6
  if (!imu.init()) {
    Serial.println("Failed to detect and initialize IMU!");
    while (1);
  }
  imu.enableDefault();
  
}

void loop() {
  imu.read();
  tilt_a = imu.a.z*0.061;

  res = map(tilt_a, -1100, 1100, 1400, 1600);
  Serial.println(res);
  servo.writeMicroseconds(res);
  delay(10);
}
