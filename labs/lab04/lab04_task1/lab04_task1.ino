
// constants won't change. Used here to set a pin number:
const int ledPin =  LED_BUILTIN;// the number of the LED pin

// Variables will change:
int ledState = LOW;             // ledState used to set the LED

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated

// constants won't change:
const long interval = 200;           // interval at which to blink (milliseconds)

bool is_blinking = false;

const int pir_pin = 5;           // passive infrared sensor pin
bool in_motion = false;



void setup(){
    Serial.begin(9600);          // initialize serial with 9600 baud rate
    pinMode(pir_pin, INPUT);     // set pin #5 as an input from PIR
    pinMode(ledPin, OUTPUT);
}


void loop(){
  
    if(digitalRead(pir_pin) == HIGH  &&  !in_motion){
      Serial.println("Motion detected");
      in_motion = true;
      if (is_blinking) {
        //Serial.println("Blinking stopped");
        is_blinking = false;
        ledState = LOW;
      }
      else {
        is_blinking = true;
      }
      
    }
    
    if(digitalRead(pir_pin) == LOW  &&  in_motion){
      Serial.println("No movement any more");
      in_motion = false;
    }
    
    
    

    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= interval) {
      // save the last time you blinked the LED
      previousMillis = currentMillis;
  
      // if the LED is off turn it on and vice-versa:
      if (is_blinking) {
        //Serial.println("Blinking started");
        if (ledState == LOW) {
          //Serial.println("Led ON");
          ledState = HIGH;
        } else {
          //Serial.println("Led OFF");
          ledState = LOW;
        }
      }
  
      // set the LED with the ledState of the variable:
      digitalWrite(ledPin, ledState);
  }
}
