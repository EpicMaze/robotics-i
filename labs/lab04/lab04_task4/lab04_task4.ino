#include <Wire.h>

#include <LIS3MDL.h>
#include <LPS.h>
#include <LSM6.h>

LIS3MDL mag;
LPS ps;
LSM6 imu;

char lis3hdl_report[80];





void setup() {
  Serial.begin(9600);
  Wire.begin();

  // LIS3HDL
  if (!mag.init()) {
    Serial.println("Failed to detect and initialize magnetometer!");
    while (1);
  }
  mag.enableDefault();

  //LPS
  if (!ps.init()) {
    Serial.println("Failed to autodetect pressure sensor!");
    while (1);
  }
  ps.enableDefault();

  //LSH6
  if (!imu.init()) {
    Serial.println("Failed to detect and initialize IMU!");
    while (1);
  }
  imu.enableDefault();



}



void loop() {

  // LIS3HDL
  mag.read();
  Serial.print("Magnetometer: \nX:");
  Serial.print(mag.m.x/6842.0);
  Serial.print(" gauss | Y: ");
  Serial.print(mag.m.y/6842.0);
  Serial.print(" gauss | Z: ");
  Serial.print(mag.m.z/6842.0);
  Serial.println(" gauss");
  Serial.println();


  //LPS

  float pressure = ps.readPressureMillibars();
  float altitude = ps.pressureToAltitudeMeters(pressure);
  float temperature = ps.readTemperatureC();

  //Serial.println("LPS");
  Serial.print("Pressure: ");
  Serial.print(pressure);
  Serial.print(" mbar | Altitude: ");
  Serial.print(altitude);
  Serial.print(" m | Temperature: ");
  Serial.print(temperature);
  Serial.println(" C");
  Serial.println();
  //delay(100);m

  //LSH6
  imu.read();
  Serial.print("Acceleration. ");
  Serial.print("X : ");
  Serial.print(imu.a.x * 0.061/1000);
  Serial.print(" g | Y : ");
  Serial.print(imu.a.y * 0.061/1000);
  Serial.print(" g | Z : ");
  Serial.print(imu.a.z * 0.061/1000);
  Serial.println(" g");

  Serial.print("Gyroscope.");
  Serial.print("X : ");
  Serial.print(imu.g.x * 8.75/1000);
  Serial.print(" dps | Y : ");
  Serial.print(imu.g.y * 8.75/1000);
  Serial.print(" dps | Z : ");
  Serial.print(imu.g.z * 8.75/1000);
  Serial.println(" dps");
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  Serial.println();
  


  delay(3000);
}
