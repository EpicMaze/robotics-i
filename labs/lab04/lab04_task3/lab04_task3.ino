#include <Servo.h>

Servo servo;

const int servo_pin = 3;
const int analogus_pin = A1;



int light_value;
int rot_pulse;
float time_delta;
float rpm;

bool is_in_shadow = false;
int in_shade_th = 50;
int out_shade_th = 120;


unsigned long previousMillis = 0;

void setup() {
  Serial.begin(9600);
  pinMode(analogus_pin, INPUT);
  servo.attach(servo_pin);


}

void loop() {

  if (Serial.available() > 0) {
    rot_pulse = Serial.parseInt();
    if (rot_pulse >= 1400 && rot_pulse <= 1600) {
      //Serial.println(rot_pulse);
      servo.writeMicroseconds(rot_pulse);
    }
    
  }


  light_value = analogRead(analogus_pin);
  //Serial.println(light_value);
  if (light_value >= out_shade_th && is_in_shadow == true) {
    is_in_shadow = false;
  }

  if (light_value <= in_shade_th && is_in_shadow == false) {
    is_in_shadow = true;
    unsigned long currentMillis = millis();
    time_delta = currentMillis - previousMillis;
    previousMillis = currentMillis;
    rpm = 60000 / time_delta;
    Serial.println(rpm);
  }
 


}
