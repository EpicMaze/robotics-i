#include <Servo.h>

Servo servo;

bool is_rotating = true;
int rot_pulse;
int garbage;
const int pir_pin = 5;           // passive infrared sensor pin
bool in_motion = false;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  servo.attach(3);
  pinMode(pir_pin, INPUT);
  Serial.println("Enter uS:");
}

void loop() {
  // put your main code here, to run repeatedly:
  if (digitalRead(pir_pin) == HIGH  &&  !in_motion) {
    Serial.println("Motion detected");
    in_motion = true;
    if (is_rotating) {
      //Serial.println("Blinking stopped");
      is_rotating = false;

    }
    else {
      is_rotating = true;
    }

  }

  if (digitalRead(pir_pin) == LOW  &&  in_motion) {
    Serial.println("No movement any more");
    in_motion = false;
  }


  if (is_rotating) {
    servo.attach(3);
    if (Serial.available() > 0) {
      rot_pulse = Serial.parseInt();
      if (rot_pulse >= 1400 && rot_pulse <= 1600) {
        //Serial.println(rot_pulse);
        servo.writeMicroseconds(rot_pulse);
      }
      else {
        Serial.println("Not in range!");
      }
    }
  }
  else {
    
    garbage = Serial.parseInt();
    servo.detach();
    Serial.println("Servo is disabled and not taking inputs");
  }

}
