#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import signal
import time
import easygopigo3 as go
import read_sensors as sensors
import sys


def follow(robot, ls1, ls2, ls3, ls4, ls5):
    """
    TASK: Code for following the line based on line sensor readings
    """
    if ls3 == 0:
        robot.forward()
    elif ls2 == 0:
        robot.left()
    elif ls4 == 0:
        robot.right()
    else:
        print("No line detected")
        
    
    return


def markers_detected(ls1, markers_count, on_marker):
    """
    TASK: Code for detecting markers, update markers_count if needed
    """
    #global on_marker
    
    
    if ls1==0 and not on_marker:
        markers_count += 1
        on_marker = True
    elif ls1 == 1:
        on_marker = False
        
    

    return markers_count, on_marker


def close(message=""):
    """
    line_following specific cleanup function
    """
    global running, ser, robot
    print(message)
    running = False
    robot.stop()
    if ser.is_open:
        ser.close()
    sys.exit(0)


def signal_handler(sig, frame):
    """
    This function will be called when CTRL+C is pressed
    """
    close('\nYou pressed Ctrl+C! Closing the program nicely :)')




def is_reach_end(markers_count, markers_all):
    if markers_count == markers_all:
        return True
    else:
        return False


if __name__ == "__main__":
    # Register a callback for CTRL+C
    signal.signal(signal.SIGINT, signal_handler)

    robot = go.EasyGoPiGo3()
    robot.set_speed(60)

    running, ser = sensors.initialize_serial('/dev/ttyUSB0')

    markers_count = 0 # Change to correct value
    markers_all = 7
    markers_dist = {0: "1900mm", 1: "1800mm", 2: "1620mm", 3:"1380mm", 4:"1120mm", 5:"700m", 6:"400m", 7: "200m"}
    on_marker = False
    while running:
        arduino_data = sensors.get_data_from_arduino(ser)
        if arduino_data:
            ls1 = arduino_data['ls1']
            ls2 = arduino_data['ls2']
            ls3 = arduino_data['ls3']
            ls4 = arduino_data['ls4']
            ls5 = arduino_data['ls5']
            
            
            markers_count, on_marker = markers_detected(ls1, markers_count, on_marker)
            
            #print("LS1: ", ls1, "LS2: ", ls2, "LS3: ", ls3, "LS4: ", ls4, "LS5: ", ls5, "count: ", markers_count, "on_marker: ", on_marker)
        
            print("Markers count: ", markers_count, "Line completed: ",markers_dist[markers_count])
           
            

            if is_reach_end(markers_count):
                robot.stop()
            else:
                follow(robot, ls1, ls2, ls3, ls4, ls5)

        if not ser.is_open:
            close("Serial is closed!")

        # Throttle the loop to 50 times per second
        time.sleep(.02)
    close()
