#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import easygopigo3 as go
import time
import signal
import cv2
import threading
import line_following as line
import numpy as np
import read_sensors as sensors
from visualisation import RepeatTimer
import sys
import sensor_fusion as fusion

# Dictionary for holding positions
positions = {'current_marker': 0, 'current_us': -1, 'current_enc': -1, 'current_cam': -1}

# Dictinoary for refering to marker distace (markercount : distance in mm)
markers_dist = {0:1900, 1:1800, 2:1620, 3:1380, 4:1120, 5:700, 6:400, 7:200}

#us_positions = []

#N = 5

markers_all = 7

def fast_worker(running, robot, positions, ser, close_function):
    """
    Fastworker logic
    A while-loop with the main control logic and should be used for fast processes.
    """

    print("Starting fastWorker in a separate thread")

    # Distance from the START marker to the wall in mm
    start_to_wall_dist = 1000
    on_marker = False
    last_ls1 = 1
    while running:
        arduino_data = sensors.get_data_from_arduino(ser)

        """
        TASK: Get the averaged encoder value and use it to
        find the distance from the wall in millimetres
        """

        

        if arduino_data:
            ls1 = arduino_data['ls1']
            ls2 = arduino_data['ls2']
            ls3 = arduino_data['ls3']
            ls4 = arduino_data['ls4']
            ls5 = arduino_data['ls5']
            us_pos = arduino_data['us']

            """
            TASK: save current ultrasonic position to positions dictionary
            """
        
            
            positions['current_enc'] = int(markers_dist.get(positions['current_marker'], 0) - robot.read_encoders_average() * 10) #to mm
            fusion.on_encoder_measurement(positions['current_enc'])
            
            positions['current_marker'], on_marker = line.markers_detected(ls1, positions['current_marker'], on_marker)
            
            if last_ls1 == 1 and ls1 == 0:
    
                robot.reset_encoders()
                
            positions['current_us'] = us_pos
            fusion.on_ultrasonic_measurement(us_pos)
            
            
            fusion.moving_average(us_pos)
            
            
            
            last_ls1 = ls1
            
            
            
            
            
            
            
            

            
            
            if line.is_reach_end(positions['current_marker'], markers_all):
                robot.stop()
            else: 
                line.follow(robot, ls1, ls2, ls3, ls4, ls5)

            
            #Add the rest of your line following & marker detection logic
            
            
            
        if not ser.is_open:
            close_function("Serial is closed!")

        # Limit control thread to 50 Hz
        time.sleep(0.02)

    close_function("Fast_worker closed!")


def detect_blobs(frame):
    """
    Image processing and blob detection logic
    """
    # Blog params
    blobparams = cv2.SimpleBlobDetector_Params()
    blobparams.minArea = 300
    blobparams.filterByArea = True
    blobparams.maxArea = 99999
    #blobparams.minDistBetweenBlobs = 100
    blobparams.filterByCircularity = False
    blobparams.filterByInertia = False
    blobparams.filterByConvexity = False
    #blobparams.filterByColor = True
    #blobparams.blobColor = 255
    
    detector = cv2.SimpleBlobDetector_create(blobparams)
    
    # Yellow
    lowerLimits = np.array([18, 128, 162])
    upperLimits = np.array([149, 224, 246])
    
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    thresholded = cv2.rectangle(thresholded, (1, 1), (frame.shape[1]-1, frame.shape[0]-1), (0, 0, 0), 3)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    
    mask = cv2.bitwise_not(outimage)
    
    keypoints = detector.detect(mask)

    return keypoints


def get_blob_size(keypoints):
    """
    Find the size of biggest keypoint
    """
    if keypoints:
        max_size = int(np.max([keypoint.size for keypoint in keypoints]))
    else:
        max_size = 0
    return max_size


def get_distance_with_cam(blob_size):
    """
    Calculate distance based on blob size
    """
    a = 130943.8972273205
    b = -211.7360320109383
    
    if blob_size > 0:
        dist = (a / blob_size) + b
    else:
        dist = -1
    
    return dist


def slow_worker():
    """
    Slower code
    Low update rate is suitable for slow processes, such as image processing, displaying data to graph, etc;
    """
    global positions

    ret, frame = cap.read()

    # Get the blob size and convert it to distance from the wall
    keypoints = detect_blobs(frame)
    blob_size = get_blob_size(keypoints)
    positions['current_cam'] = get_distance_with_cam(blob_size)

    print(positions)
    print("BLOB SIZE: ", blob_size)
    image_with_keypoints = cv2.drawKeypoints(frame, keypoints, np.array([]), (0, 0, 255),
                                             cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    cv2.imshow("Camera image", image_with_keypoints)
    if cv2.waitKey(1) == 27:
        close("Image closed")


# This function will be called when CTRL+C is pressed
def signal_handler(sig, frame):
    """
    This function will be called when CTRL+C is pressed, read_sensors specific
    """
    close('\nYou pressed Ctrl+C! Closing the program nicely :)')


def close(message=""):
    """
    localization_logic specific cleanup
    """
    global running, ser, robot, timer
    print(message)
    running = False
    robot.stop()
    if ser.is_open:
        ser.close()
    timer.cancel()
    if fast_thread.is_alive:
        try:
            fast_thread.join()
        except:
            pass
    sys.exit(0)


if __name__ == "__main__":
    # Register a callback for CTRL+C
    signal.signal(signal.SIGINT, signal_handler)

    running, ser = sensors.initialize_serial('/dev/ttyUSB0')

    robot = go.EasyGoPiGo3()
    robot.set_speed(60)

    # Open the camera
    cap = cv2.VideoCapture(0)

    # Create fast_worker in a separate thread.
    fast_thread = threading.Thread(
        target=fast_worker,
        args=(running, robot, positions, ser, close)
    )
    fast_thread.daemon = True
    fast_thread.start()

    timer = RepeatTimer(0.1, slow_worker)
    timer.start()

    while running:
        time.sleep(1)
    close()
