#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import cv2
import easygopigo3 as go
import numpy as np
import time

# Global variable for determining GoPiGo speed.
gospeed = 150

# Global variable for video feed.
cap = None

# Global variable for robot object.
pi = go.EasyGoPiGo3()

FRAME_XCENTER = None

def init():
    global cap, gospeed
    # This function should do everything required to initialize the robot.
    # Among other things it should open the camera and set GoPiGo speed.
    # Some of this has already been filled in.
    # You are welcome to add your own code if needed.

    cap = cv2.VideoCapture(0)
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

    pi.set_speed(gospeed)
    return


# TASK 1
def get_line_location(frame):
    # This function should use a single frame from the camera to determine line location.
    # It should return the location of the line in the frame.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    
    #Threshold values
    
    lowerLimits = np.array([71, 78, 99])
    upperLimits = np.array([96, 255, 156])
    
    
    # Thresholding frame
    frame_cut = frame[350:370, : , :]
    frame_hsv = cv2.cvtColor(frame_cut, cv2.COLOR_BGR2HSV)
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    #outimage = cv2.bitwise_and(frame_cut, frame_cut, mask = thresholded)
    
    #cv2.imshow("th", thresholded)
    
    # Calculate horizontal coords of the line
    y_coords, x_coords = np.nonzero(thresholded)
    #xy_coords = list(zip(x_coords, y_coords))
    linelocation = np.mean(x_coords) if len(x_coords) != 0 else 0
    

    return int(linelocation)


# TASK 2
def bang_bang(linelocation):
    # This function should use the line location to implement a simple bang-bang controller.
    # YOUR CODE HERE
    
    if linelocation < FRAME_XCENTER:
        pi.left()
    elif linelocation > FRAME_XCENTER:
        pi.right()
        
    return


# TASK 3
def bang_bang_improved(linelocation):
    # This function should use the line location to implement an improved version of the bang-bang controller.
    # YOUR CODE HERE
    print("CENTER", FRAME_XCENTER)
    x_error = 90
    
    if linelocation < FRAME_XCENTER - x_error:
        pi.left()
    elif linelocation > FRAME_XCENTER + x_error:
        pi.right()
    else:
        pi.forward()
    
    return


# TASK 4
def proportional_controller(linelocation):
    # This function should use the line location to implement a proportional controller.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    #print(gospeed)
    error = FRAME_XCENTER - linelocation
    Kp = 0.5
    P = error * Kp
    lwheel = gospeed - P
    rwheel = gospeed + P
    
    pi.set_motor_dps(pi.MOTOR_LEFT, lwheel)
    pi.set_motor_dps(pi.MOTOR_RIGHT, rwheel)
    
    return



# Trackbr for Ku
#cv2.namedWindow("tunning_trackbar")
#cv2.createTrackbar("Ku", "tunning_trackbar", 0, 300, (lambda a: None))


time_start = 0
error_prev = 0
error_integral = 0
# TASK 5
def pid_controller(linelocation):
    # This function should use the line location to implement a PID controller.
    # Feel free to define and use any global variables you may need.
    # YOUR CODE HERE
    global time_start, error_prev, error_integral
    
    if (time_start != 0):
        time_end = time.time()
        time_delta = time_end - time_start
    else:
        time_delta = 0.0001
        
    time_start = time.time()
    
    
    # ----- Errors -----
    #
    error = FRAME_XCENTER - linelocation
    error_integral += error * time_delta
    error_derv = (error - error_prev) / time_delta
    error_prev = error
    # -------------------
    
    # ----- Contstants --------
    #
    # Tuniing
    
    # getting float values for more preccision.
    #Ku = cv2.getTrackbarPos("Ku", "tunning_trackbar") / 100
    Ku = 1.27
    
    #Ki = Kd = 0
    
    # Tu = 12/7
    
    
    Tu = 1.71
    
    # After tunning
    
    Kp = 0.6 * Ku
    Ki = (1.2 * Ku)/Tu
    Kd = (3 * Ku * Tu)/40
    
    # ---------------------------
    
    u = Kp * error + Ki * error_integral + Kd * error_derv
    
    lwheel = gospeed - u
    rwheel = gospeed + u
    
    
    pi.set_motor_dps(pi.MOTOR_LEFT, lwheel)
    pi.set_motor_dps(pi.MOTOR_RIGHT, rwheel)
    
    
    return


# Initialization
init()

try:
    while True:
        # We read information from the camera.
        ret, frame = cap.read()
        FRAME_XCENTER = int(frame.shape[1]/2)
        #cv2.imshow('Original', frame[350:370,:,:])

        # Task 1: uncomment the following line and implement get_line_location function.
        # DONE
        linelocation = get_line_location(frame)

        print('Linelocation:' ,linelocation)
        # Task 2: uncomment the following line and implement bang_bang function.
        # DONE
        #bang_bang(linelocation)

        # Task 3: uncomment the following line and implement bang_bang_improved function.
        # DONE
        #bang_bang_improved(linelocation)

        # Task 4: uncomment the following line and implement proportional_controller function.
        # DONE
        #proportional_controller(linelocation)

        # Task 5: uncomment the following line and implement pid_controller function.
        #pid_controller(linelocation)

        if cv2.waitKey(1) == 27:
            break

except KeyboardInterrupt:
    cap.release()
    cv2.destroyAllWindows()
    pi.stop()

cap.release()
cv2.destroyAllWindows()
pi.stop()

