import cv2
import numpy as np


cap = cv2.VideoCapture(0)

# Colour detection limits initial
lH = 0
lS = 0
lV = 0
hH = 179
hS = 255
hV = 255


# Trackbar values
trackbar_max_val = 255
trackbar_H = "H"
trackbar_S = "S"
trackbar_V = "V"
trackbar_H_max = "H MAx"
trackbar_S_max = "S Max"
trackbar_V_max = "V MAx"
trackbar_window = "trackbar"

# Trackbars create
cv2.namedWindow(trackbar_window)
cv2.createTrackbar(trackbar_H, trackbar_window, lH, 179, (lambda a: None))
cv2.createTrackbar(trackbar_S, trackbar_window, lS, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V, trackbar_window, lV, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_H_max, trackbar_window, hH, 179, (lambda a: None))
cv2.createTrackbar(trackbar_S_max, trackbar_window, hS, trackbar_max_val, (lambda a: None))
cv2.createTrackbar(trackbar_V_max, trackbar_window, hV, trackbar_max_val, (lambda a: None))



while True:
    
    ret, frame = cap.read()
    
    # Get trackbar vals
    lH = cv2.getTrackbarPos(trackbar_H, trackbar_window)
    lS = cv2.getTrackbarPos(trackbar_S, trackbar_window)
    lV = cv2.getTrackbarPos(trackbar_V, trackbar_window)
    
    hH = cv2.getTrackbarPos(trackbar_H_max, trackbar_window)
    hS = cv2.getTrackbarPos(trackbar_S_max, trackbar_window)
    hV = cv2.getTrackbarPos(trackbar_V_max, trackbar_window)
    
    # Limits and inRange
    frame_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lowerLimits = np.array([lH, lS, lV])
    upperLimits = np.array([hH, hS, hV])
    thresholded = cv2.inRange(frame_hsv, lowerLimits, upperLimits)
    outimage = cv2.bitwise_and(frame, frame, mask = thresholded)
    
    
    cv2.imshow(trackbar_window, outimage)
    #cv2.imshow("Original", frame)
    
    
    if cv2.waitKey(1) == 27:
        break
    
    
print('closing program')
cv2.destroyAllWindows() 
    